# swowpy

Simple wrapper for openweathermap for Python. Fully compatible with Renpy engine.
Swowpy is licensed as free/libre software :)

## How to install it

 ```
 pip install swowpy
 ```
 ### Integration with renpy
 Go to the game folder:
 
 ```python
 pip install --target game/python-packages swowpy
 ```
 And import in script.py
 ```python
 init python:
    import swowpy
 ```
 
 ## How to use it
 
### Create a swowpy object

```python
api_key = "0a0a0a0a0a0a0a"
mycity = swowpy.Swowpy(api_key,"Granada")
```

### Get weather
It returns "Sun","Clouds"...

```python
mycity.current_weather()
mycity.current_weather("Description") # Provides a description 
```

### Get temperature

```python
mycity.temperature() #Get temperature in Kelvin
mycity.temperature(unit = "Celsius") #Get temperature in Celsius
```

### Get wind speed and direction
Returns a tuple with wind speed and wind direction.
```python
mycity.wind() #For example (8.2,240) 
```

### Other methods
humidity() and pressure() are also available.raw() method return the whole JSON response from OpenWeather.



